<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Share;
use App\Repository\ShareRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class ShareController extends AbstractController
{
    /**
     * @Route("/article/{id}/share", name="share_store", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Article $article
     * @param ShareRepository $shareRepository
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function store(Article $article, ShareRepository $shareRepository, UserInterface $user): JsonResponse
    {
        $share = $shareRepository->findOneByArticleAndUser($article, $user);

        if (! empty($share)) {
            return $this->json('Share already exists', 400);
        }

        $like = new Share();

        $like->setUser($user);
        $like->setArticle($article);
        $like->setCreatedAt(new \DateTime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($like);
        $entityManager->flush();

        return $this->json('Share successfully stored', 201);
    }

    /**
     * @Route("/article/{id}/share", name="share_destroy", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @param Article $article
     * @param ShareRepository $shareRepository
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function destroy(Article $article, ShareRepository $shareRepository, UserInterface $user): JsonResponse
    {
        $share = $shareRepository->findOneByArticleAndUser($article, $user);

        if (empty($share)) {
            return $this->json('Share not found', 404);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($share[0]);
        $entityManager->flush();

        return $this->json('Share successfully deleted', 200);
    }
}
