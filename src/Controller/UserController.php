<?php

namespace App\Controller;

use App\Form\UserFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user/home", name="user_home")
     * @IsGranted("ROLE_USER")
     * @param UserInterface $user
     * @return Response
     */
    public function index(UserInterface $user): Response
    {
        $likes = $user->getLikes();
        $shares = $user->getShares();
        return $this->render('user/home.html.twig', [
            'likes' => $likes,
            'shares' => $shares,
        ]);
    }

    /**
     * @Route("/user/show", name="user_show")
     * @IsGranted("ROLE_USER")
     * @param UserInterface $user
     * @param Request $request
     * @return Response
     */
    public function show(UserInterface $user, Request $request): Response
    {
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'Vos informations personnelles ont été modifiées avec succès.');
        }

        return $this->render('user/show.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
