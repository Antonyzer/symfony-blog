<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/home", name="admin_home")
     * @param ArticleRepository $articleRepository
     * @param UserInterface $user
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(ArticleRepository $articleRepository, UserInterface $user): Response
    {
        $articles = $articleRepository->getLastFiveByUser($user);

        return $this->render('admin/home.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/admin/articles", name="admin_articles")
     * @param ArticleRepository $articleRepository
     * @param UserInterface $user
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function articles(ArticleRepository $articleRepository, UserInterface $user): Response
    {
        $articles = $articleRepository->getAdminArticles($user);

        return $this->render('admin/articles.html.twig', [
            'articles' => $articles
        ]);
    }
}
