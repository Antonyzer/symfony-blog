<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleFormType;
use App\Form\CommentFormType;
use App\Repository\ArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function index(ArticleRepository $articleRepository): Response
    {
        return $this->render('article/index.html.twig', [
            'articles' => $articleRepository->findAllActive(),
        ]);
    }

    /**
     * @Route("/article/{id}", name="article_show", methods={"GET", "POST"})
     * @param Article $article
     * @param ArticleRepository $articleRepository
     * @param Request $request
     * @return Response
     */
    public function show(Article $article, ArticleRepository $articleRepository, Request $request): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentFormType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $comment->setStatus('waiting');
            $comment->setArticle($article);
            $comment->setUser($this->getUser());
            $comment->setCreatedAt(new \DateTime());
            $comment->setUpdatedAt(new \DateTime());

            $entityManager->persist($comment);
            $entityManager->flush();

            $this->addFlash('success', 'Votre commentaire a été pris en compte, l\'adminsitrateur doit valider votre commentaire pour qu\'il soit visible.');
        }

        return $this->render('article/show.html.twig', [
            'article' => $article,
            'articles' => $articleRepository->findLastFour(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/article", name="article_store", methods={"GET", "POST"})
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @param UserInterface $user
     * @param SluggerInterface $slugger
     * @return Response
     */
    public function store(Request $request, UserInterface $user, SluggerInterface $slugger): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleFormType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imgFile = $form->get('img_loc')->getData();

            $origFilename = pathinfo($imgFile->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($origFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$imgFile->guessExtension();

            try {
                $imgFile->move(
                    $this->getParameter('img_directory'),
                    $newFilename
                );
            } catch (FileException $e) {

            }

            $entityManager = $this->getDoctrine()->getManager();
            $article->setCreatedAt(new \DateTime());
            $article->setUpdatedAt(new \DateTime());
            $article->setImgLoc($newFilename);
            if ($article->getStatus()) {
                $article->setPublishedAt(new \DateTime());
            }
            $article->setUser($user);

            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('success', 'Votre article a été ajouté avec succès.');
            return $this->redirectToRoute('home');
        }

        return $this->render('article/store.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/article/{id}/update", name="article_update", methods={"GET", "PUT"})
     * @IsGranted("ROLE_ADMIN")
     * @param Article $article
     * @param Request $request
     * @param UserInterface $user
     * @return Response
     */
    public function update(Article $article, Request $request, UserInterface $user): Response
    {
        $form = $this->createForm(ArticleFormType::class, $article, [
            'method' => 'PUT'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $article->setUpdatedAt(new \DateTime());
            $article->setUser($user);

            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('success', 'Votre article a été mise à jour avec succès.');
            return $this->redirectToRoute('admin_articles');
        }

        return $this->render('article/update.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/article/{id}", name="article_destroy", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     * @param Article $article
     * @return Response
     */
    public function destroy(Article $article): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();

        return $this->redirectToRoute('admin_articles');
    }
}
