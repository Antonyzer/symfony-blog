<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Like;
use App\Repository\LikeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class LikeController extends AbstractController
{
    /**
     * @Route("/article/{id}/like", name="like_store", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Article $article
     * @param LikeRepository $likeRepository
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function store(Article $article, LikeRepository $likeRepository, UserInterface $user): JsonResponse
    {
        $like = $likeRepository->findOneByArticleAndUser($article, $user);

        if (! empty($like)) {
            return $this->json('Like already exists', 400);
        }

        $like = new Like();

        $like->setUser($user);
        $like->setArticle($article);
        $like->setCreatedAt(new \DateTime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($like);
        $entityManager->flush();

        return $this->json('Like successfully stored', 201);
    }

    /**
     * @Route("/article/{id}/like", name="like_destroy", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @param Article $article
     * @param LikeRepository $likeRepository
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function destroy(Article $article, LikeRepository $likeRepository, UserInterface $user): JsonResponse
    {
        $like = $likeRepository->findOneByArticleAndUser($article, $user);

        if (empty($like)) {
            return $this->json('Like not found', 404);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($like[0]);
        $entityManager->flush();

        return $this->json('Like successfully deleted', 200);
    }
}
