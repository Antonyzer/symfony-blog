<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    /**
     * @Route("/article/{id}/comment", name="comment_index")
     * @IsGranted("ROLE_ADMIN")
     * @param Article $article
     * @return Response
     */
    public function index(Article $article): Response
    {
        return $this->render('comment/index.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/comment/{id}/{status}", name="comment_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     * @param Comment $comment
     * @param string $status
     * @return Response
     */
    public function update(Comment $comment, string $status): Response
    {
        $statuses = [
            'waiting',
            'accepted',
            'refused',
        ];

        if (! in_array($status, $statuses)) {
            return $this->json('Status not found', 404);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $comment->setStatus($status);
        $entityManager->persist($comment);
        $entityManager->flush();

        return $this->json('Comment successfully updated', 200);
    }
}
