<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $admin = $this->getReference('user_admin');

        $tags = [
            'starwarsfan',
            'disney',
            'jedi',
            'themandalorian',
            'cosplay',
            'yoda',
            'obiwan',
            'stormtrooper'
        ];

        $imgs = [
            'babyyoda.jpg',
            'stormtroopers.jpg',
            'darkvador.jpg',
            'yoda.jpg',
            'c3po.jpg',
        ];

        for ($i = 0; $i < 20; $i++) {
            $article = new Article();
            $current_date = new \DateTime();
            $current_date->modify('+' . $i . ' days');

            $article_tags = [];
            for ($j = 0; $j < 3; $j++) {
                array_push(
                    $article_tags,
                    $tags[random_int(0, sizeof($tags) - 1)]
                );
            }

            $article
                ->setUser($admin)
                ->setTitle($this->faker->words(5, true))
                ->setContent($this->faker->paragraphs(3, true))
                ->setSummary($this->faker->sentence(4, true))
                ->setStatus(true)
                ->setCategory($this->getReference('category_' . random_int(0, 8)))
                ->setReadingTime(random_int(2, 10))
                ->setTags($article_tags)
                ->setImgLoc($imgs[random_int(0, sizeof($imgs) - 1)])
                ->setCreatedAt($current_date)
                ->setUpdatedAt($current_date)
                ->setPublishedAt($current_date);

            $manager->persist($article);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            UserFixtures::class,
            CategoryFixtures::class,
        );
    }
}
