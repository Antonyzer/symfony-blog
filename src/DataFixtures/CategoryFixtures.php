<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $categories = [
            'A New Hope',
            'The Empire Strikes Back',
            'Return of the Jedi',
            'The Phantom Menace',
            'Attack of the Clones',
            'Revenge of the Sith',
            'The Force Awakens',
            'The Last Jedi',
            'The Rise of Skywalker',
        ];

        for ($i = 0; $i < sizeof($categories); $i++) {
            $category = new Category();
            $category
                ->setName($categories[$i]);
            $manager->persist($category);
            $this->addReference('category_' . $i, $category);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            UserFixtures::class,
        );
    }
}
