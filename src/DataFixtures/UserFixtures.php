<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin
            ->setEmail('admin@mail.fr')
            ->setRoles(['ROLE_ADMIN'])
            ->setPassword($this->encoder->encodePassword($admin, 'admin'))
            ->setFirstname('Anakin')
            ->setLastname('Skywalker')
            ->setName('Dark Vador')
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());

        $manager->persist($admin);
        $this->addReference('user_admin', $admin);

        $user = new User();
        $user
            ->setEmail('user@mail.fr')
            ->setPassword($this->encoder->encodePassword($user,'user'))
            ->setFirstname('Luke')
            ->setLastname('Skywalker')
            ->setName('TheJedi')
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());

        $manager->persist($user);
        $this->addReference('user_user', $user);

        $manager->flush();
    }
}
