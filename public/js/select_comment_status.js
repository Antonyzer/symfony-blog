$(document).ready(function () {
   $('.status').on('change', function () {
       const status = $(this).val();
       const commentId = $(this).data('comment');
       const statuses = [
           'waiting',
           'accepted',
           'refused',
       ];

       if (statuses.includes(status)) {
           $.ajax({
               url: '/comment/' + commentId + '/' + status,
               method: 'PUT',
               success: function (response) {
                   console.log(response);
               }
           });
       }
   });
});