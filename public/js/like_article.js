$(document).ready(function () {
    $('.like').on('click', function () {
        // Get current article id
        const articleId = $(this).data('article');
        const action = $(this)[0].dataset['action'];
        const spanCount = $('#likeCount' + articleId);
        const buttonAction = $(".like[data-article='" + articleId + "']")[0];

        if (action === 'add') {
            $.ajax({
                url: '/article/' + articleId + '/like',
                type: 'POST',
                success: function (response) {
                    // Change img
                    $('#like' + articleId).attr('src', '/img/heart_full.png');
                    // Increment likes count
                    let count = spanCount.html();
                    spanCount.html(parseInt(count) + 1);
                    // Change button action
                    buttonAction.dataset['action'] = 'remove';
                }
            });
        } else if (action === 'remove') {
            $.ajax({
                url: '/article/' + articleId + '/like',
                type: 'DELETE',
                success: function (response) {
                    // Change img
                    $('#like' + articleId).attr('src', '/img/heart_empty.png');
                    // Increment likes count
                    let count = spanCount.html();
                    spanCount.html(parseInt(count) - 1);
                    // Change button action
                    buttonAction.dataset['action'] = 'add';
                }
            });
        }
    });
});