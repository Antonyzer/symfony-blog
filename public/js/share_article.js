$(document).ready(function () {
    $('.share').on('click', function () {
        const articleId = $(this).data('article');
        const action = $(this)[0].dataset['action'];
        const spanCount = $('#shareCount' + articleId);
        const buttonAction = $(".share[data-article='" + articleId + "']")[0];

        if (action === 'add') {
            $.ajax({
                url: '/article/' + articleId + '/share',
                type: 'POST',
                success: function (response) {
                    // Change img
                    $('#share' + articleId).attr('src', '/img/share_success.png');
                    // Increment likes count
                    let count = spanCount.html();
                    spanCount.html(parseInt(count) + 1);
                    // Change button action
                    buttonAction.dataset['action'] = 'remove';
                }
            });
        } else if (action === 'remove') {
            $.ajax({
                url: '/article/' + articleId + '/share',
                type: 'DELETE',
                success: function (response) {
                    // Change img
                    $('#share' + articleId).attr('src', '/img/share.png');
                    // Increment likes count
                    let count = spanCount.html();
                    spanCount.html(parseInt(count) - 1);
                    // Change button action
                    buttonAction.dataset['action'] = 'add';
                }
            });
        }
    });
});