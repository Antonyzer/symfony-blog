$(document).ready(function () {
    $('.remove').on('click', function () {
        // Get current article id
        const articleId = $(this).data('article');
        const url = window.location.href;
        const urlArray = url.split('/');

        $.ajax({
            url: '/article/' + articleId,
            type: 'DELETE',
            success: function (response) {
                if (urlArray[urlArray.length - 1] === 'update') {
                    window.location.href = '/admin/articles';
                } else {
                    location.reload();
                }
            }
        });
    });
});